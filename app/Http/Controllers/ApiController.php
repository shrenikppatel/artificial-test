<?php

namespace App\Http\Controllers;

use Dingo\Api\Routing\Helpers;

/**
 * Class ApiController
 * @package App\Http\Controllers
 */
class ApiController extends Controller
{
    use Helpers; //Dingo Api helpers for better response management
}
