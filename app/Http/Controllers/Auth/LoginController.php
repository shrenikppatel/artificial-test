<?php declare(strict_types=1);

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\ApiController;
use App\Http\Requests\LoginRequest;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\JWTAuth;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Class LoginController
 * @package App\Http\Controllers\Auth
 */
class LoginController extends ApiController
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | responding with a JWT token.
    |
    */

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var JWTAuth
     */
    protected $auth;


    /**
     * LoginController constructor.
     * @param Request $request
     */
    public function __construct(Request $request, JWTAuth $auth)
    {
        $this->request = $request;
        $this->auth = $auth;
    }

    /**
     * @param LoginRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function authenticate(LoginRequest $request)
    {
        // Grab credentials from the request
        $credentials = $request->only('email', 'password');

        try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = $this->auth->attempt($credentials)) {
                throw new AccessDeniedHttpException("Invalid credentials");
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            throw new HttpException("Could not create token");
        }

        // all good so return the token
        return response()->json(compact('token'));
    }

    /**
     * Refreshes the JWT token
     * @return mixed
     */
    public function refreshToken()
    {
        $new_token = $this->auth->setRequest($this->request)->parseToken()->refresh();

        // send the refreshed token back to the client
        return response(['token' => $new_token])->header("Authorization", 'Bearer '.$new_token);
    }
}
