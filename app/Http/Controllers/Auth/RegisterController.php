<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\ApiController;
use Artificial\Domain\Users\Exceptions\EmailTakenException;
use Artificial\Domain\Users\Exceptions\RegistrationException;
use Artificial\Domain\Users\Exceptions\UsernameTakenException;
use Artificial\Domain\Users\Services\RegisterUserService;
use Dingo\Api\Exception\StoreResourceFailedException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

/**
 * Class RegisterController
 * @package App\Http\Controllers\Auth
 */
class RegisterController extends ApiController
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users.
    |
    */

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var RegisterUserService
     */
    protected $register_service;

    /**
     * RegisterController constructor.
     * @param Request $request
     * @param RegisterUserService $register_service
     */
    public function __construct(Request $request, RegisterUserService $register_service)
    {
        $this->request = $request;
        $this->register_service = $register_service;
    }

    /**
     * Handles registration route
     * @return mixed
     */
    public function register()
    {
        $validator = Validator::make($this->request->all(), [
            'first_name' => 'required|max:100',
            'last_name' => 'required|max:100',
            'username' => 'required|alpha_dash|min:3',
            'email' => 'required|email',
            'password' => 'required|alpha_dash|min:6',
            'address' => 'required|max:100',
            'phone' => 'required'
        ]);

        if ($validator->fails()) {
            throw new StoreResourceFailedException(trans("registration.failed"), $validator->errors());
        }

        try {
            $user = $this->register_service->handle($this->request->all());
        } catch (EmailTakenException $e) {
            throw new StoreResourceFailedException(trans("registration.failed"), ['email' => $e->getMessage()]);
        } catch (UsernameTakenException $e) {
            throw new StoreResourceFailedException(trans("registration.failed"), ['username' => $e->getMessage()]);
        } catch (RegistrationException $e) {
            throw new StoreResourceFailedException(trans("registration.failed"));
        }

        return $this->response->created();
    }
}
