<?php declare(strict_types=1);

namespace App\Http\Controllers;

use Artificial\Domain\Users\Exceptions\EmailTakenException;
use Artificial\Domain\Users\Exceptions\InvalidPasswordException;
use Artificial\Domain\Users\Exceptions\UsernameTakenException;
use Artificial\Domain\Users\Exceptions\UserUpdateException;
use Artificial\Domain\Users\Services\PasswordChangeService;
use Artificial\Domain\Users\Services\UserDeletionService;
use Artificial\Domain\Users\Services\UserUpdateService;
use Dingo\Api\Exception\UpdateResourceFailedException;
use Illuminate\Http\Request;
use App\Http\Transformers\UserTransformer;
use Illuminate\Support\Facades\Validator;

/**
 * Class UserController
 * @package App\Http\Controllers\Auth
 */
class UserController extends ApiController
{
    /*
    |--------------------------------------------------------------------------
    | User Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the user routes for the application.
    |
    */

    /**
     * @var Request
     */
    protected $request;


    /**
     * UserController constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Display a current user
     * @return \Dingo\Api\Http\Response
     */
    public function show()
    {
        $user = $this->request->user();
        return $this->response->item($user, new UserTransformer);
    }

    /**
     * Update user details
     * @param UserUpdateService $service
     * @return \Dingo\Api\Http\Response
     */
    public function update(UserUpdateService $service)
    {
        $validator = Validator::make($this->request->all(), [
            'first_name' => 'required|max:100',
            'last_name' => 'required|max:100',
            'username' => 'required|alpha_dash|min:3',
            'email' => 'required|email',
            'address' => 'required|max:100',
            'phone' => 'required'
        ]);

        if ($validator->fails()) {
            throw new UpdateResourceFailedException(trans("user.update.failed"), $validator->errors());
        }

        try {
            $service->handle($this->request->user(), $this->request->all());
        } catch (EmailTakenException $e) {
            throw new UpdateResourceFailedException(trans("user.update.failed"), ['email' => $e->getMessage()]);
        } catch (UsernameTakenException $e) {
            throw new UpdateResourceFailedException(trans("user.update.failed"), ['username' => $e->getMessage()]);
        } catch (UserUpdateException $e) {
            throw new UpdateResourceFailedException(trans("user.update.failed"));
        }

        return $this->response->noContent();
    }


    /**
     * @param PasswordChangeService $service
     * @return \Dingo\Api\Http\Response
     */
    public function updatePassword(PasswordChangeService $service)
    {
        $validator = Validator::make($this->request->all(), [
            'current_password' => 'required',
            'new_password' => 'required|alpha_dash|min:6'
        ]);

        if ($validator->fails()) {
            throw new UpdateResourceFailedException(trans("user.update.failed"), $validator->errors());
        }

        try {
            $service->handle(
                $this->request->user(),
                (string) $this->request->get('current_password'),
                (string) $this->request->get('new_password')
            );
        } catch (InvalidPasswordException $e) {
            throw new UpdateResourceFailedException(trans("user.update.failed"), [
                'current_password' => $e->getMessage()
            ]);
        } catch (UserUpdateException $e) {
            throw new UpdateResourceFailedException(trans("user.update.failed"), [
                'new_password' => $e->getMessage()
            ]);
        }

        return $this->response->noContent();
    }

    /**
     * @param UserDeletionService $service
     * @return \Dingo\Api\Http\Response
     */
    public function delete(UserDeletionService $service)
    {
        $service->handle($this->request->user());

        return $this->response->noContent();
    }
}
