<?php declare(strict_types=1);

namespace App\Http\Transformers;

use Artificial\Domain\Users\Models\User;
use League\Fractal\TransformerAbstract;

/**
 * Class UserTransformer
 * @package App\Http\Transformers
 */
class UserTransformer extends TransformerAbstract
{
    /**
     * @param User $user
     * @return array
     */
    public function transform(User $user) : array
    {
        return [
            'id'            => (int) $user->id,
            'first_name'    => (string) $user->first_name,
            'last_name'     => (string) $user->last_name,
            'email'         => (string) $user->email,
            'username'      => (string) $user->username,
            'phone'         => (string) $user->phone,
            'address'       => (string) $user->address,
            'created_at'    => (string) $user->created_at->toDateTimeString(),
            'updated_at'    => (string) $user->updated_at->toDateTimeString()
        ];
    }
}
