<?php

namespace App\Http\Middleware;

use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Middleware\GetUserFromToken;

class ApiAuth extends GetUserFromToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, \Closure $next)
    {
        if (! $token = $this->auth->setRequest($request)->getToken()) {
            throw new BadRequestHttpException("token_not_provided");
        }

        try {
            $user = $this->auth->authenticate($token);
        } catch (TokenExpiredException $e) {
            throw new BadRequestHttpException('token_expired');
        } catch (JWTException $e) {
            throw new BadRequestHttpException("token_invalid");
        }

        if (! $user) {
            throw new BadRequestHttpException("token_invalid");
        }

        $this->events->fire('tymon.jwt.valid', $user);

        return $next($request);
    }
}
