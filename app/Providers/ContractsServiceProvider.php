<?php

namespace App\Providers;

use Artificial\Domain\Users\Contracts\UserRepository;
use Artificial\Infrastructure\DbUserRepository;
use Illuminate\Support\ServiceProvider;

class ContractsServiceProvider extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;


    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //Will bind UserRepository to DbUserRepository to use Database implementation
        $this->app->bind(
            UserRepository::class,
            DbUserRepository::class
        );
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [
            UserRepository::class
        ];
    }
}
