<?php declare(strict_types=1);

namespace Artificial\Domain\Users\Exceptions;

/**
 * Class EmailTakenException
 * @package Artificial\Domain\Users\Exceptions
 */
class EmailTakenException extends RegistrationException
{
}