<?php declare(strict_types=1);

namespace Artificial\Domain\Users\Exceptions;

use DomainException;

/**
 * Class UsernameTakenException
 * @package Artificial\Domain\Users\Exceptions
 */
class UsernameTakenException extends DomainException
{
}