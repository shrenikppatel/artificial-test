<?php declare(strict_types=1);

namespace Artificial\Domain\Users\Exceptions;

use DomainException;

/**
 * Thrown when a user is not found
 * Class UserNotFoundException
 * @package Artificial\Domain\Users\Exceptions
 */
class UserNotFoundException extends DomainException
{
}