<?php declare(strict_types=1);

namespace Artificial\Domain\Users\Exceptions;

use DomainException;

/**
 * Class UserUpdateException
 * @package Artificial\Domain\Users\Exceptions
 */
class UserUpdateException extends DomainException
{
}