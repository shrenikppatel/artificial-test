<?php declare(strict_types=1);

namespace Artificial\Domain\Users\Exceptions;

use DomainException;

/**
 * Class InvalidPasswordException
 * @package Artificial\Domain\Users\Exceptions
 */
class InvalidPasswordException extends DomainException
{
}