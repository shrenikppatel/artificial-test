<?php declare(strict_types=1);

namespace Artificial\Domain\Users\Exceptions;

use DomainException;

/**
 * Class RegistrationException
 * @package Artificial\Domain\Users\Exceptions
 */
class RegistrationException extends DomainException
{
}