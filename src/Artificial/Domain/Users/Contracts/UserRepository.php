<?php declare(strict_types=1);

namespace Artificial\Domain\Users\Contracts;

use Artificial\Domain\Users\Models\User;
use Artificial\Domain\Users\ValueObjects\Address;
use Artificial\Domain\Users\ValueObjects\Email;
use Artificial\Domain\Users\ValueObjects\FirstName;
use Artificial\Domain\Users\ValueObjects\LastName;
use Artificial\Domain\Users\ValueObjects\Password;
use Artificial\Domain\Users\ValueObjects\Phone;
use Artificial\Domain\Users\ValueObjects\Username;
use Mockery\Generator\StringManipulation\Pass\Pass;

/**
 * Interface UserRepository
 * @package Artificial\Domain\Users\Contracts
 */
interface UserRepository
{
    /** Creates a new user in the data store
     * @param FirstName $first_name
     * @param LastName $last_name
     * @param Username $username
     * @param Phone $phone
     * @param Address $address
     * @param Email $email
     * @param Password $password
     * @return User
     */
    public function create(
        FirstName $first_name,
        LastName $last_name,
        Username $username,
        Phone $phone,
        Address $address,
        Email $email,
        Password $password
    ) : User;

    /**
     * Update the user details in the data store
     * @param FirstName $first_name
     * @param LastName $last_name
     * @param Username $username
     * @param Phone $phone
     * @param Address $address
     * @param Email $email
     */
    public function update(
        User $user,
        FirstName $first_name,
        LastName $last_name,
        Username $username,
        Phone $phone,
        Address $address,
        Email $email
    ) : void;

    /**
     * Gets the user by its id
     * @param int $id
     * @return User
     */
    public function findById(int $id) : User;


    /**
     * @param Email $email
     * @param int|null $except_Id
     * @return bool
     */
    public function doesEmailExist(Email $email, ?int $except_Id = null) : bool;


    /**
     * @param Username $username
     * @param int|null $except_id
     * @return bool
     */
    public function doesUsernameExist(Username $username, ?int $except_id = null) : bool;

    /**
     * @param User $user
     * @param Password $password
     * @return void
     */
    public function updatePassword(User $user, Password $password) : void;

    /**
     * Deletes the user
     * @param User $user
     */
    public function delete(User $user) : void;
}