<?php declare(strict_types=1);

namespace Artificial\Domain\Users\ValueObjects;

use BadMethodCallException;
use InvalidArgumentException;

/**
 * Class Username
 * @package Artificial\Domain\Users
 */
class Username
{
    /**
     * @var string
     */
    private $username;

    /**
     * Username constructor.
     * @param string $username
     */
    public function __construct(string $username)
    {
        $this->validateUsername($username);
        $this->username = $username;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->username;
    }

    /**
     * Making the class immutable
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {
        throw new BadMethodCallException("Username ValueObject is immutable");
    }

    /**
     * Validates the username
     * @param $username
     */
    private function validateUsername($username)
    {
        if (str_contains($username, " ")) {
            throw new InvalidArgumentException("Username can't have spaces");
        }

        if (strlen($username) < 3) {
            throw new InvalidArgumentException("Username needs to be at least 3 characters");
        }
    }
}