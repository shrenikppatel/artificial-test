<?php declare(strict_types=1);

namespace Artificial\Domain\Users\ValueObjects;

use InvalidArgumentException;
use BadMethodCallException;

/**
 * Class Email
 * @package Artificial\Domain\Users
 */
class Email
{
    /**
     * @var string
     */
    private $email;

    /**
     * Email constructor.
     * @param string $email
     */
    public function __construct(string $email)
    {
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $this->email = $email;
        } else {
            throw new InvalidArgumentException("Email is not in a valid format");
        }
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->email;
    }

    /**
     * Making the class immutable
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {
        throw new BadMethodCallException("Email ValueObject is immutable");
    }
}
