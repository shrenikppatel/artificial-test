<?php declare(strict_types=1);

namespace Artificial\Domain\Users\ValueObjects;

use BadMethodCallException;

/**
 * Phone ValueObject
 * Class Phone
 * @package Artificial\Domain\Users
 */
class Phone
{
    /**
     * @var string
     */
    private $phone;

    /**
     * Phone constructor.
     * @param string $phone
     */
    public function __construct(string $phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->phone;
    }

    /**
     * Making the class immutable
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {
        throw new BadMethodCallException("Phone ValueObject is immutable");
    }
}
