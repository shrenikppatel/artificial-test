<?php declare(strict_types=1);

namespace Artificial\Domain\Users\ValueObjects;

use BadMethodCallException;
use InvalidArgumentException;

/**
 * Class Password
 * @package Artificial\Domain\Users
 */
class Password
{
    /**
     * @var string
     */
    private $password;

    /**
     * Password constructor.
     * @param string $password
     */
    public function __construct(string $password)
    {
        $this->validatePassword($password);
        $this->password = $password;
    }

    /**
     * @param $password
     */
    private function validatePassword($password)
    {
        if (str_contains($password, " ")) {
            throw new InvalidArgumentException("Password cannot contain spaces");
        }

        if (strlen($password) < 6) {
            throw new InvalidArgumentException("Password needs to be at least 6 characters in length");
        }
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->password;
    }

    /**
     * Returns the password hash
     * @return string
     */
    public function hash() : string
    {
        return bcrypt($this->password);
    }

    /**
     * Making the class immutable
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {
        throw new BadMethodCallException("Password ValueObject is immutable");
    }
}
