<?php declare(strict_types=1);

namespace Artificial\Domain\Users\ValueObjects;

use InvalidArgumentException;
use BadMethodCallException;

/**
 * FirstName ValueObject
 * Class FirstName
 * @package Artificial\Domain\Users
 */
class FirstName
{
    /**
     * @var string
     */
    private $first_name;

    /**
     * Address constructor.
     * @param string $address
     */
    public function __construct(string $first_name)
    {
        if (strlen($first_name) > 100) {
            throw new InvalidArgumentException("First Name needs to be less than 100 characters");
        }
        $this->first_name = $first_name;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->first_name;
    }

    /**
     * Making the class immutable
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {
        throw new BadMethodCallException("FirstName ValueObject is immutable");
    }
}
