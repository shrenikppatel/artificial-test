<?php declare(strict_types=1);

namespace Artificial\Domain\Users\ValueObjects;

use InvalidArgumentException;
use BadMethodCallException;

/**
 * LastName ValueObject
 * Class LastName
 * @package Artificial\Domain\Users
 */
class LastName
{
    /**
     * @var string
     */
    private $last_name;

    /**
     * Address constructor.
     * @param string $address
     */
    public function __construct(string $last_name)
    {
        if (strlen($last_name) > 100) {
            throw new InvalidArgumentException("Last Name needs to be less than 100 characters");
        }
        $this->last_name = $last_name;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->last_name;
    }

    /**
     * Making the class immutable
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {
        throw new BadMethodCallException("LastName ValueObject is immutable");
    }
}
