<?php declare(strict_types=1);

namespace Artificial\Domain\Users\ValueObjects;

use InvalidArgumentException;
use BadMethodCallException;

/**
 * Address ValueObject
 * Class Address
 * @package Artificial\Domain\Users
 */
class Address
{
    /**
     * @var string
     */
    private $address;

    /**
     * Address constructor.
     * @param string $address
     */
    public function __construct(string $address)
    {
        if (strlen($address) > 100) {
            throw new InvalidArgumentException("Address needs to be less than 100 characters");
        }
        $this->address = $address;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->address;
    }

    /**
     * Making the class immutable
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {
        throw new BadMethodCallException("Address ValueObject is immutable");
    }
}
