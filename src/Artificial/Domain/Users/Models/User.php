<?php declare(strict_types=1);

namespace Artificial\Domain\Users\Models;

use Artificial\Domain\Users\ValueObjects\Address;
use Artificial\Domain\Users\ValueObjects\Email;
use Artificial\Domain\Users\ValueObjects\FirstName;
use Artificial\Domain\Users\ValueObjects\LastName;
use Artificial\Domain\Users\ValueObjects\Password;
use Artificial\Domain\Users\ValueObjects\Phone;
use Artificial\Domain\Users\ValueObjects\Username;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class User
 * @package Artificial\Domain\Users\Models
 */
class User extends Authenticatable
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
        'first_name',
        'last_name',
        'phone',
        'address',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password'
    ];

    /**
     * @param Username $username
     */
    public function setUsernameAttribute(Username $username): void
    {
        $this->attributes['username'] = (string)$username;
    }

    /**
     * @param $value
     * @return Username
     */
    public function getUsernameAttribute($value): Username
    {
        return new Username($value);
    }

    /**
     * @param FirstName $first_name
     */
    public function setFirstNameAttribute(FirstName $first_name): void
    {
        $this->attributes['first_name'] = (string)$first_name;
    }

    /**
     * @param $value
     * @return FirstName
     */
    public function getFirstNameAttribute($value): FirstName
    {
        return new FirstName($value);
    }

    /**
     * @param LastName $last_name
     */
    public function setLastNameAttribute(LastName $last_name): void
    {
        $this->attributes['last_name'] = (string)$last_name;
    }

    /**
     * @param $value
     * @return LastName
     */
    public function getLastNameAttribute($value): LastName
    {
        return new LastName($value);
    }

    /**
     * @param Phone $phone
     */
    public function setPhoneAttribute(Phone $phone): void
    {
        $this->attributes['phone'] = (string)$phone;
    }

    /**
     * @param $value
     * @return Phone
     */
    public function getPhoneAttribute($value): Phone
    {
        return new Phone($value);
    }

    /**
     * @param Address $address
     */
    public function setAddressAttribute(Address $address): void
    {
        $this->attributes['address'] = (string)$address;
    }

    /**
     * @param $value
     * @return Address
     */
    public function getAddressAttribute($value): Address
    {
        return new Address($value);
    }

    /**
     * @param Email $email
     */
    public function setEmailAttribute(Email $email): void
    {
        $this->attributes['email'] = (string)$email;
    }

    /**
     * @param $value
     * @return Email
     */
    public function getEmailAttribute($value): Email
    {
        return new Email($value);
    }

    /**
     * @param Password $password
     */
    public function setPasswordAttribute(Password $password): void
    {
        $this->attributes['password'] = $password->hash();
    }

    /**
     * @param $value
     * @return string
     */
    public function getPasswordAttribute($value): string
    {
        return $value;
    }
}
