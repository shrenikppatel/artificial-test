<?php declare(strict_types=1);

namespace Artificial\Domain\Users\Services;

use Artificial\Domain\Users\Contracts\UserRepository;
use Artificial\Domain\Users\Models\User;

/**
 * Class UserDeletionService
 * @package Artificial\Domain\Users\Services
 */
class UserDeletionService
{
    /**
     * @var UserRepository
     */
    protected $user_repo;


    /**
     * UserDeletionService constructor.
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->user_repo = $repository;
    }


    /**
     * Deletes the user
     * @param User $user
     */
    public function handle(User $user) : void
    {
        $this->user_repo->delete($user);
    }
}