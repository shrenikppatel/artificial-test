<?php declare(strict_types=1);

namespace Artificial\Domain\Users\Services;

use Artificial\Domain\Users\Contracts\UserRepository;
use Artificial\Domain\Users\Exceptions\EmailTakenException;
use Artificial\Domain\Users\Exceptions\RegistrationException;
use Artificial\Domain\Users\Exceptions\UsernameTakenException;
use Artificial\Domain\Users\Models\User;
use Artificial\Domain\Users\ValueObjects\Address;
use Artificial\Domain\Users\ValueObjects\Email;
use Artificial\Domain\Users\ValueObjects\FirstName;
use Artificial\Domain\Users\ValueObjects\LastName;
use Artificial\Domain\Users\ValueObjects\Password;
use Artificial\Domain\Users\ValueObjects\Phone;
use Artificial\Domain\Users\ValueObjects\Username;
use InvalidArgumentException;

/**
 * Class RegisterUserService
 * @package Artificial\Domain\Users\Services
 */
class RegisterUserService
{
    /**
     * @var UserRepository
     */
    protected $user_repo;

    /**
     * RegisterUserService constructor.
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->user_repo = $repository;
    }

    /**
     * @param array $data
     * @return User
     */
    public function handle(array $data) : User
    {
        //Validate input fields and convert to value objects
        try {
            $email = new Email($data['email']);
            $username = new Username($data['username']);
            $last_name = new LastName($data['last_name']);
            $first_name = new FirstName($data['first_name']);
            $address = new Address($data['address']);
            $phone = new Phone($data['phone']);
            $password = new Password($data['password']);
        } catch (InvalidArgumentException $e) {
            throw new RegistrationException($e->getMessage());
        }

        //Validate uniqueness of email and username
        $this->validateIfUniqueEmail($email);
        $this->validateIfUniqueUsername($username);

        $user = $this->user_repo->create($first_name, $last_name, $username, $phone, $address, $email, $password);

        return $user;
    }

    /**
     * @param Email $email
     */
    private function validateIfUniqueEmail(Email $email) : void
    {
        if ($this->user_repo->doesEmailExist($email)) {
            throw new EmailTakenException(trans('registration.email.exists'));
        }
    }

    /**
     * @param Username $username
     */
    private function validateIfUniqueUsername(Username $username) : void
    {
        if ($this->user_repo->doesUsernameExist($username)) {
            throw new UsernameTakenException(trans('registration.username.exists'));
        }
    }
}