<?php declare(strict_types=1);

namespace Artificial\Domain\Users\Services;

use Artificial\Domain\Users\Contracts\UserRepository;
use Artificial\Domain\Users\Exceptions\InvalidPasswordException;
use Artificial\Domain\Users\Exceptions\UserUpdateException;
use Artificial\Domain\Users\Models\User;
use Artificial\Domain\Users\ValueObjects\Password;
use InvalidArgumentException;

/**
 * Class PasswordChangeService
 * @package Artificial\Domain\Users\Services
 */
class PasswordChangeService
{
    /**
     * @var UserRepository
     */
    protected $user_repo;

    /**
     * RegisterUserService constructor.
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->user_repo = $repository;
    }


    /**
     * @param User $user
     * @param string $current_password
     * @param string $new_password
     */
    public function handle(User $user, string $current_password, string $new_password) : void
    {
        $this->checkCurrentPasswordValidity($user, $current_password);

        try {
            $password = new Password($new_password);
            $this->user_repo->updatePassword($user, $password);
        } catch (InvalidArgumentException $e) {
            throw new UserUpdateException($e->getMessage());
        }
    }

    /**
     * @param User $user
     * @param string $current_password
     */
    private function checkCurrentPasswordValidity(User $user, string $current_password)
    {
        if (! password_verify($current_password, $user->password)) {
            throw new InvalidPasswordException(trans('user.current_password.incorrect'));
        }
    }
}