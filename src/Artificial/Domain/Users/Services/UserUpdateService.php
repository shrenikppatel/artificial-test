<?php declare(strict_types=1);

namespace Artificial\Domain\Users\Services;

use Artificial\Domain\Users\Contracts\UserRepository;
use Artificial\Domain\Users\Exceptions\EmailTakenException;
use Artificial\Domain\Users\Exceptions\UsernameTakenException;
use Artificial\Domain\Users\Exceptions\UserUpdateException;
use Artificial\Domain\Users\Models\User;
use Artificial\Domain\Users\ValueObjects\Address;
use Artificial\Domain\Users\ValueObjects\Email;
use Artificial\Domain\Users\ValueObjects\FirstName;
use Artificial\Domain\Users\ValueObjects\LastName;
use Artificial\Domain\Users\ValueObjects\Phone;
use Artificial\Domain\Users\ValueObjects\Username;
use InvalidArgumentException;

/**
 * Class UserUpdateService
 * @package Artificial\Domain\Users\Services
 */
class UserUpdateService
{
    /**
     * @var UserRepository
     */
    protected $user_repo;


    /**
     * UserUpdateService constructor.
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->user_repo = $repository;
    }

    /**
     * Updates the user's details
     * @param array $data
     * @return void
     */
    public function handle(User $user, array $data) : void
    {
        //Validate input fields and convert to value objects
        try {
            $email = new Email($data['email']);
            $username = new Username($data['username']);
            $last_name = new LastName($data['last_name']);
            $first_name = new FirstName($data['first_name']);
            $address = new Address($data['address']);
            $phone = new Phone($data['phone']);
        } catch (InvalidArgumentException $e) {
            throw new UserUpdateException($e->getMessage());
        }

        //Validate uniqueness of email and username
        $this->validateIfUniqueEmail($email, $user->id);
        $this->validateIfUniqueUsername($username, $user->id);

        $this->user_repo->update($user, $first_name, $last_name, $username, $phone, $address, $email);
    }

    /**
     * @param Email $email
     */
    private function validateIfUniqueEmail(Email $email, int $id) : void
    {
        if ($this->user_repo->doesEmailExist($email, $id)) {
            throw new EmailTakenException(trans('registration.email.exists'));
        }
    }

    /**
     * @param Username $username
     */
    private function validateIfUniqueUsername(Username $username, int $id) : void
    {
        if ($this->user_repo->doesUsernameExist($username, $id)) {
            throw new UsernameTakenException(trans('registration.username.exists'));
        }
    }
}
