<?php declare(strict_types=1);

namespace Artificial\Infrastructure;

use Artificial\Domain\Users\Contracts\UserRepository;
use Artificial\Domain\Users\Exceptions\UserNotFoundException;
use Artificial\Domain\Users\Models\User;
use Artificial\Domain\Users\ValueObjects\Address;
use Artificial\Domain\Users\ValueObjects\Email;
use Artificial\Domain\Users\ValueObjects\FirstName;
use Artificial\Domain\Users\ValueObjects\LastName;
use Artificial\Domain\Users\ValueObjects\Password;
use Artificial\Domain\Users\ValueObjects\Phone;
use Artificial\Domain\Users\ValueObjects\Username;
use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
 * Class DbUserRepository
 * @package Artificial\Infrastructure
 */
class DbUserRepository implements UserRepository
{
    /**
     * @param FirstName $first_name
     * @param LastName $last_name
     * @param Username $username
     * @param Phone $phone
     * @param Address $address
     * @param Email $email
     * @param Password $password
     * @return User
     */
    public function create(
        FirstName $first_name,
        LastName $last_name,
        Username $username,
        Phone $phone,
        Address $address,
        Email $email,
        Password $password
    ): User {
        $data = [
            'first_name' => $first_name,
            'last_name' => $last_name,
            'username' => $username,
            'phone' => $phone,
            'address' => $address,
            'email' => $email,
            'password' => $password
        ];

        $user = User::create($data);

        return $user;
    }

    /**
     * @param User $user
     * @param FirstName $first_name
     * @param LastName $last_name
     * @param Username $username
     * @param Phone $phone
     * @param Address $address
     * @param Email $email
     */
    public function update(
        User $user,
        FirstName $first_name,
        LastName $last_name,
        Username $username,
        Phone $phone,
        Address $address,
        Email $email
    ): void {

        $user->first_name = $first_name;
        $user->last_name = $last_name;
        $user->username = $username;
        $user->phone = $phone;
        $user->address = $address;
        $user->email = $email;
        $user->save();
    }

    /**
     * @param int $id
     * @return User
     */
    public function findById(int $id): User
    {
        try {
            return User::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            throw new UserNotFoundException("User was not found");
        }
    }


    /**
     * @param Email $
     * @param int|null $except_id
     * @return bool
     */
    public function doesEmailExist(Email $email, ?int $except_id = null) : bool
    {
        $query_builder = User::where('email', (string) $email);

        if ($except_id) {
            $query_builder->where('id', '!=', $except_id);
        }

        $user = $query_builder->first();

        if ($user) {
            return true;
        } else {
            return false;
        }
    }


    /**
     * @param Username $username
     * @param int|null $except_id
     * @return bool
     */
    public function doesUsernameExist(Username $username, ?int $except_id = null) : bool
    {
        $query_builder = User::where('username', (string)$username);

        //Omit id if provided
        if ($except_id) {
            $query_builder->where('id', '!=', $except_id);
        }

        $user = $query_builder->first();

        if ($user) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param User $user
     * @param Password $password
     */
    public function updatePassword(User $user, Password $password) : void
    {
        $user->password = $password;
        $user->save();
    }

    /**
     * Deletes the user from the data store
     * @param User $user
     */
    public function delete(User $user): void
    {
        $user->delete();
    }
}
