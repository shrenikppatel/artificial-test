<?php

return [

    /*
    |--------------------------------------------------------------------------
    | User Language Lines
    |--------------------------------------------------------------------------
    |
    */

    'update' => [
        'failed' => "Could not update the user."
    ],
    'current_password' => [
        'incorrect' => "Wrong password"
    ]

];
