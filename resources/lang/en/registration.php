<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Registration Language Lines
    |--------------------------------------------------------------------------
    |
    */

    'email' => [
        'exists' => "Another user with the same email exists.",
    ],
    'username' => [
        'exists' => "Username is taken.",
    ],
    'failed' => "Could not register a new user."

];
