<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
*/

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
    //Login using the user's credentials and issue a JWT token
    $api->post('auth/login', ['uses' => 'App\Http\Controllers\Auth\LoginController@authenticate']);

    //Refresh the JWT token
    $api->get('auth/refresh', [
        'middleware' => 'jwt.auth',
        'uses' => 'App\Http\Controllers\Auth\LoginController@refreshToken'
    ]);

    //Register a new user
    $api->post('auth/register', [
        'uses' => 'App\Http\Controllers\Auth\RegisterController@register'
    ]);

    //Fetch user details
    $api->get('user', [
        'middleware' => 'jwt.auth',
        'uses' => 'App\Http\Controllers\UserController@show'
    ]);

    //Update user details
    $api->put('user', [
        'middleware' => 'jwt.auth',
        'uses' => 'App\Http\Controllers\UserController@update'
    ]);

    //Update user password
    $api->put('user/password', [
        'middleware' => 'jwt.auth',
        'uses' => 'App\Http\Controllers\UserController@updatePassword'
    ]);

    //Deletes the user
    $api->delete('user', [
        'middleware' => 'jwt.auth',
        'uses' => 'App\Http\Controllers\UserController@delete'
    ]);
});
