<?php

namespace Tests\Feature;

use Artificial\Domain\Users\Models\User;
use Faker\Factory;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

/**
 * Class UserControllerTest
 * @package Tests\Feature
 */
class UserControllerTest extends TestCase
{
    use DatabaseMigrations, DatabaseTransactions;

    /**
     * @test
     */
    public function itShouldReturnAUserObject()
    {
        $user = factory(User::class)->create();

        $token = $this->makeToken($user);

        $response = $this->get('/api/user', ['Authorization' => 'Bearer '.$token]);

        $response->assertStatus(200);

        $response->assertExactJson([
            "data" => [
                'id'            => (int) $user->id,
                'first_name'    => (string) $user->first_name,
                'last_name'     => (string) $user->last_name,
                'email'         => (string) $user->email,
                'username'      => (string) $user->username,
                'phone'         => (string) $user->phone,
                'address'       => (string) $user->address,
                'created_at'    => (string) $user->created_at->toDateTimeString(),
                'updated_at'    => (string) $user->updated_at->toDateTimeString()
            ]
        ]);
    }

    /** @test */
    public function itShouldUpdateUserDetails()
    {
        $faker = Factory::create();

        $user = factory(User::class)->create();

        $token = $this->makeToken($user);

        $data = [
            'first_name' => $faker->firstName,
            'last_name' => $faker->lastName,
            'address' => $faker->address,
            'phone' => $faker->phoneNumber,
            'email' => $faker->email,
            'username' => "myusername",
        ];

        $response = $this->put("/api/user", $data, ['Authorization' => "Bearer $token"]);
        $response->assertStatus(204);
    }

    /**
     * @test
     */
    public function testUpdateValidationRequiredRulesResponse()
    {
        $faker = Factory::create();

        $user = factory(User::class)->create();

        $token = $this->makeToken($user);

        $data = [
            'first_name' => "",
            'last_name' => "",
            'address' => "",
            'phone' => "",
            'email' => "",
            'username' => "",
        ];

        $response = $this->put("/api/user", $data, ['Authorization' => "Bearer $token"]);

        $response->assertStatus(422);

        $response->assertJson([
            "errors" => [
                'first_name' => true,
                'last_name' => true,
                'address' => true,
                'phone' => true,
                'email' => true,
                'username' => true
            ]
        ]);
    }

    /**
     * @test
     */
    public function testUpdateValidationWithFieldRulesResponse()
    {
        $faker = Factory::create();

        $user = factory(User::class)->create();

        $token = $this->makeToken($user);

        $data = [
            'first_name' => str_random(101),
            'last_name' => str_random(101),
            'address' => str_random(101),
            'phone' => phoneStub(),
            'email' => "notanemail",
            'username' => "contains space"
        ];

        $response = $this->put("/api/user", $data, ['Authorization' => "Bearer $token"]);

        $response->assertStatus(422);

        $response->assertJson([
            "errors" => [
                'first_name' => true,
                'last_name' => true,
                'address' => true,
                'email' => true,
                'username' => true
            ]
        ]);
    }

    /**
     * @test
     */
    public function testRegisterValidationWithTakenEmail()
    {
        $user = factory(User::class)->create();
        $faker = Factory::create();
        $token = $this->makeToken($user);
        $taken_email_user = factory(User::class)->create(['email' => emailStub("john.doe@mail.com")]);

        $data = [
            'first_name' => $faker->firstName,
            'last_name' => $faker->lastName,
            'address' => $faker->address,
            'phone' => $faker->phoneNumber,
            'email' => (string) $taken_email_user->email,
            'username' => "myusername",
            'password' => str_random(6)
        ];

        $response = $this->put("/api/user", $data, ['Authorization' => "Bearer $token"]);

        $response->assertStatus(422);

        $response->assertJson([
            "errors" => [
                'email' => true,
            ]
        ]);
    }

    /**
     * @test
     */
    public function testRegisterValidationWithTakenUsername()
    {
        $user = factory(User::class)->create();
        $faker = Factory::create();
        $token = $this->makeToken($user);
        $taken_username_user = factory(User::class)->create(['username' => usernameStub(str_random(6))]);

        $data = [
            'first_name' => $faker->firstName,
            'last_name' => $faker->lastName,
            'address' => $faker->address,
            'phone' => $faker->phoneNumber,
            'email' => $faker->email,
            'username' => (string) $taken_username_user->username
        ];

        $response = $this->put("/api/user", $data, ['Authorization' => "Bearer $token"]);

        $response->assertStatus(422);

        $response->assertJson([
            "errors" => [
                'username' => true,
            ]
        ]);
    }

    /**
     * @test
     */
    public function testUpdatePassword()
    {
        $user = factory(User::class)->create(['password' => passwordStub(123456)]);
        $token = $this->makeToken($user);

        $data = [
            'current_password' => 123456,
            'new_password' => 567890
        ];

        $response = $this->put("/api/user/password", $data, ['Authorization' => "Bearer $token"]);

        $response->assertStatus(204);
    }

    /**
     * @test
     */
    public function testUpdatePasswordWithInvalidCurrentPassword()
    {
        $user = factory(User::class)->create(['password' => passwordStub(123456)]);
        $token = $this->makeToken($user);

        $data = [
            'current_password' => 12345,
            'new_password' => 567890
        ];

        $response = $this->put("/api/user/password", $data, ['Authorization' => "Bearer $token"]);
        $response->assertStatus(422);

        $response->assertJson([
            "errors" => [
                'current_password' => true,
            ]
        ]);
    }

    /**
     * @test
     */
    public function testUpdatePasswordValidationRules()
    {
        $user = factory(User::class)->create(['password' => passwordStub(123456)]);
        $token = $this->makeToken($user);

        $data = [
            'current_password' => "",
            'new_password' => "^%^$3"
        ];

        $response = $this->put("/api/user/password", $data, ['Authorization' => "Bearer $token"]);

        $response->assertStatus(422);

        $response->assertJson([
            "errors" => [
                'current_password' => true,
                'new_password' => true
            ]
        ]);
    }

    /**
     * @test
     */
    public function testDelete()
    {
        $user = factory(User::class)->create();
        $token = $this->makeToken($user);

        $response = $this->delete("/api/user", [], ['Authorization' => "Bearer $token"]);

        $response->assertStatus(204);

        $this->assertSoftDeleted('users', ['id' => $user->id]);
    }

}
