<?php

namespace Tests\Feature;

use Artificial\Domain\Users\Models\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tymon\JWTAuth\Facades\JWTAuth;

/**
 * Class JWTAuthTest
 * @package Tests\Feature
 */
class JWTAuthTest extends TestCase
{
    use DatabaseMigrations, DatabaseTransactions;

    /**
     *
     * @return void
     */
    public function testJWTAuthWithNoToken()
    {
        $response = $this->get("/api/auth/refresh");

        $response->assertStatus(400);

        $response->assertJson(['message' => "token_not_provided", "status_code" => 400]);
    }

    /**
     * @test
     */
    public function testJWTAuthWithInvalidToken()
    {
        $user = factory(User::class)->create();

        $token = $this->makeToken($user);

        //Invalidate token
        JWTAuth::invalidate($token);

        $response = $this->get("/api/auth/refresh", ['Authorization' => 'Bearer '.$token]);

        $response->assertStatus(400);

        $response->assertJson(['message' => "token_invalid", "status_code" => 400]);
    }
}
