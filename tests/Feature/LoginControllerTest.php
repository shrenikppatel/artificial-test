<?php

namespace Tests\Feature;

use Artificial\Domain\Users\Models\User;
use Artificial\Domain\Users\ValueObjects\Password;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

/**
 * Class LoginControllerTest
 * @package Tests\Feature
 */
class LoginControllerTest extends TestCase
{
    use DatabaseMigrations, DatabaseTransactions;

    /**
     *
     * @return void
     */
    public function testLogin()
    {
        $password = new Password('123456');
        $user = factory(User::class)->create(['password' => $password]);

        $response = $this->post('/api/auth/login', [
            'email' => (string) $user->email,
            'password' => (string) $password
        ]);

        $response->assertStatus(200);

        $response->assertJson(['token' => true]);
    }

    /**
     * @test
     */
    public function testLoginWithInvalidCredentials()
    {
        $password = new Password('123456');
        $user = factory(User::class)->create(['password' => $password]);

        $response = $this->post('/api/auth/login', [
            'email' => (string) $user->email,
            'password' => str_random(7)
        ]);

        $response->assertStatus(403);

        $response->assertJson(['message' => "Invalid credentials", "status_code" => 403]);
    }

    /**
     * @test
     */
    public function testRefreshToken()
    {
        $user = factory(User::class)->create();

        $token = $this->makeToken($user);

        $response = $this->get("/api/auth/refresh", ['Authorization' => 'Bearer '.$token]);

        $response->assertStatus(200);

        $response->assertJson(['token' => true]);
    }
}
