<?php

namespace Tests\Feature;

use Artificial\Domain\Users\Models\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Faker\Factory;

/**
 * Class RegisterControllerTest
 * @package Tests\Feature
 */
class RegisterControllerTest extends TestCase
{
    use DatabaseMigrations, DatabaseTransactions;

    /**
     *
     * @return void
     */
    public function testRegisterWithValidData()
    {
        $faker = Factory::create();

        $data = [
            'first_name' => $faker->firstName,
            'last_name' => $faker->lastName,
            'address' => $faker->address,
            'phone' => $faker->phoneNumber,
            'email' => $faker->email,
            'username' => "myusername",
            'password' => str_random(6)
        ];

        $response = $this->post("/api/auth/register", $data);

        $response->assertStatus(201);
    }

    /**
     * @test
     */
    public function testRegisterValidationRequiredRulesResponse()
    {
        $faker = Factory::create();

        $data = [
            'first_name' => "",
            'last_name' => "",
            'address' => "",
            'phone' => "",
            'email' => "",
            'username' => "",
            'password' => ""
        ];

        $response = $this->post("/api/auth/register", $data);

        $response->assertStatus(422);

        $response->assertJson([
           "errors" => [
               'first_name' => true,
               'last_name' => true,
               'address' => true,
               'phone' => true,
               'email' => true,
               'username' => true,
               'password' => true
           ]
        ]);
    }

    /**
     * @test
     */
    public function testRegisterValidationWithFieldRulesResponse()
    {
        $faker = Factory::create();

        $data = [
            'first_name' => str_random(101),
            'last_name' => str_random(101),
            'address' => str_random(101),
            'phone' => phoneStub(),
            'email' => "notanemail",
            'username' => "contains space",
            'password' => "less"
        ];

        $response = $this->post("/api/auth/register", $data);

        $response->assertStatus(422);

        $response->assertJson([
            "errors" => [
                'first_name' => true,
                'last_name' => true,
                'address' => true,
                'email' => true,
                'username' => true,
                'password' => true
            ]
        ]);
    }

    /**
     * @test
     */
    public function testRegisterValidationWithTakenEmail()
    {
        $user = factory(User::class)->create();

        $faker = Factory::create();

        $data = [
            'first_name' => $faker->firstName,
            'last_name' => $faker->lastName,
            'address' => $faker->address,
            'phone' => $faker->phoneNumber,
            'email' => (string) $user->email,
            'username' => "myusername",
            'password' => str_random(6)
        ];

        $response = $this->post("/api/auth/register", $data);

        $response->assertStatus(422);

        $response->assertJson([
            "errors" => [
                'email' => true,
            ]
        ]);
    }

    /**
     * @test
     */
    public function testRegisterValidationWithTakenUsername()
    {
        $user = factory(User::class)->create();

        $faker = Factory::create();

        $data = [
            'first_name' => $faker->firstName,
            'last_name' => $faker->lastName,
            'address' => $faker->address,
            'phone' => $faker->phoneNumber,
            'email' => $faker->email,
            'username' => (string) $user->username,
            'password' => str_random(6)
        ];

        $response = $this->post("/api/auth/register", $data);

        $response->assertStatus(422);

        $response->assertJson([
            "errors" => [
                'username' => true,
            ]
        ]);
    }
}
