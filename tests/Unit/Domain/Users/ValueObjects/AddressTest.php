<?php

namespace Tests\Unit;

use Artificial\Domain\Users\ValueObjects\Address;
use Tests\TestCase;
use InvalidArgumentException;

/**
 * Class AddressTest
 * @package Tests\Unit
 */
class AddressTest extends TestCase
{
    /**
     * @test
     */
    public function testAddressValueObject()
    {
        $string = str_random(56);

        $address = new Address($string);

        $this->assertEquals($string, (string) $address);
    }

    /**
     * @test
     */
    public function testAddressWithInvalidLength()
    {
        $string = str_random(101);

        $this->expectException(InvalidArgumentException::class);

        new Address($string);
    }
}
