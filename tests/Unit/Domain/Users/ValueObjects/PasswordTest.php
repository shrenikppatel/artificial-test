<?php

namespace Tests\Unit;

use Artificial\Domain\Users\ValueObjects\Password;
use Tests\TestCase;
use InvalidArgumentException;
use Faker\Factory;

/**
 * Class PasswordTest
 * @package Tests\Unit
 */
class PasswordTest extends TestCase
{
    /**
     * @test
     */
    public function testPasswordValueObject()
    {
        $faker = Factory::create();

        $string = $faker->password;

        $password = new Password($string);

        $this->assertEquals($string, (string) $password);
    }

    /**
     * @test
     */
    public function testPasswordWithInvalidLength()
    {
        $string = "lol";

        $this->expectException(InvalidArgumentException::class);

        new Password($string);
    }

    /**
     * @test
     */
    public function testPasswordWithSpace()
    {
        $string = "this is a new password";

        $this->expectException(InvalidArgumentException::class);

        new Password($string);
    }

    /**
     * @test
     */
    public function testPasswordHash()
    {
        $faker = Factory::create();

        $string = $faker->password;

        $password = new Password($string);

        $isValidHash = password_verify($string, $password->hash());

        $this->assertEquals(true, $isValidHash);
    }
}
