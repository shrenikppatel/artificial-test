<?php

namespace Tests\Unit;

use Artificial\Domain\Users\ValueObjects\Username;
use Tests\TestCase;
use InvalidArgumentException;
use Faker\Factory;

/**
 * Class UsernameTest
 * @package Tests\Unit
 */
class UsernameTest extends TestCase
{
    /**
     * @test
     */
    public function testWithValidUsername()
    {
        $faker = Factory::create();

        $value = "my_username";

        $username = new Username($value);

        $this->assertInstanceOf(Username::class, $username);
        $this->assertEquals($value, (string) $username);
    }

    /**
     * @test
     */
    public function testWithUsernameLessThan3Characters()
    {
        $value = str_random(2);

        $this->expectException(InvalidArgumentException::class);

        new Username($value);
    }

    /**
     * @test
     */
    public function testWithUsernameWithSpace()
    {
        $value = "this is a new username";

        $this->expectException(InvalidArgumentException::class);

        new Username($value);
    }
}
