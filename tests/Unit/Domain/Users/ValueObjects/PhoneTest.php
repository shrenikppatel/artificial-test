<?php

namespace Tests\Unit;

use Artificial\Domain\Users\ValueObjects\Phone;
use Tests\TestCase;
use Faker\Factory;

/**
 * Class PhoneTest
 * @package Tests\Unit
 */
class PhoneTest extends TestCase
{
    /**
     * @test
     */
    public function testPhoneValueObject()
    {
        $faker = Factory::create();

        $string = $faker->phoneNumber;

        $phone = new Phone($string);

        $this->assertEquals($string, (string) $phone);
    }
}
