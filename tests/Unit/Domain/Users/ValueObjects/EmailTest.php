<?php

namespace Tests\Unit;

use Artificial\Domain\Users\ValueObjects\Email;
use Tests\TestCase;
use InvalidArgumentException;
use Faker\Factory;

/**
 * Class EmailTest
 * @package Tests\Unit
 */
class EmailTest extends TestCase
{
    /**
     * @test
     */
    public function testWithValidEmail()
    {
        $value = with(Factory::create())->email;

        $email = new Email($value);

        $this->assertInstanceOf(Email::class, $email);
        $this->assertEquals($value, (string) $email);
    }

    /**
     * @test
     */
    public function testWithInvalidEmail()
    {
        $value = str_random(10);

        $this->expectException(InvalidArgumentException::class);

        new Email($value);
    }
}
