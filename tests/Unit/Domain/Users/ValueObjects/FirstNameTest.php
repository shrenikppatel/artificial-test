<?php

namespace Tests\Unit;

use Artificial\Domain\Users\ValueObjects\FirstName;
use Tests\TestCase;
use InvalidArgumentException;
use Faker\Factory;

/**
 * Class FirstNameTest
 * @package Tests\Unit
 */
class FirstNameTest extends TestCase
{
    /**
     * @test
     */
    public function testFirstNameValueObject()
    {
        $faker = Factory::create();

        $string = $faker->firstName;

        $first_name = new FirstName($string);

        $this->assertEquals($string, (string) $first_name);
    }

    /**
     * @test
     */
    public function testAddressWithInvalidLength()
    {
        $string = str_random(101);

        $this->expectException(InvalidArgumentException::class);

        new FirstName($string);
    }
}
