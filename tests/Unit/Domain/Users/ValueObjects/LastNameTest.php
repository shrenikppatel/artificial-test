<?php

namespace Tests\Unit;

use Artificial\Domain\Users\ValueObjects\LastName;
use Tests\TestCase;
use InvalidArgumentException;
use Faker\Factory;

/**
 * Class LastNameTest
 * @package Tests\Unit
 */
class LastNameTest extends TestCase
{
    /**
     * @test
     */
    public function testLastNameValueObject()
    {
        $faker = Factory::create();

        $string = $faker->lastName;

        $last_name = new LastName($string);

        $this->assertEquals($string, (string) $last_name);
    }

    /**
     * @test
     */
    public function testAddressWithInvalidLength()
    {
        $string = str_random(101);

        $this->expectException(InvalidArgumentException::class);

        new LastName($string);
    }
}
