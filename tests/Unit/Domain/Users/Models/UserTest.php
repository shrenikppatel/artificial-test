<?php

namespace Tests\Unit;

use Artificial\Domain\Users\Models\User;
use Artificial\Domain\Users\ValueObjects\Address;
use Artificial\Domain\Users\ValueObjects\Email;
use Artificial\Domain\Users\ValueObjects\FirstName;
use Artificial\Domain\Users\ValueObjects\LastName;
use Artificial\Domain\Users\ValueObjects\Phone;
use Artificial\Domain\Users\ValueObjects\Username;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\DatabaseMigrations;

/**
 * Class UserTest
 * @package Tests\Unit
 */
class UserTest extends TestCase
{
    use DatabaseTransactions, DatabaseMigrations;

    /**
     * @test
     */
    public function testUserMutators()
    {
        $data = [
            'first_name' => firstNameStub(),
            'last_name' => lastNameStub(),
            'username' => usernameStub(),
            'phone' => phoneStub(),
            'address' => addressStub(),
            'email' => emailStub(),
            'password' => passwordStub()
        ];

        $user = User::create($data);

        $this->assertInstanceOf(User::class, $user);
    }

    /**
     * @test
     */
    public function testAccessors()
    {
        $user = factory(User::class)->create();

        $this->assertInstanceOf(FirstName::class, $user->first_name);
        $this->assertInstanceOf(LastName::class, $user->last_name);
        $this->assertInstanceOf(Phone::class, $user->phone);
        $this->assertInstanceOf(Username::class, $user->username);
        $this->assertInstanceOf(Address::class, $user->address);
        $this->assertInstanceOf(Email::class, $user->email);
    }
}
