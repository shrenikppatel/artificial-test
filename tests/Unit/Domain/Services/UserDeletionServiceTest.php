<?php

namespace Tests\Unit;

use Artificial\Domain\Users\Contracts\UserRepository;
use Artificial\Domain\Users\Services\UserDeletionService;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Mockery;
use Artificial\Domain\Users\Models\User;

class UserDeletionServiceTest extends TestCase
{
    use DatabaseMigrations, DatabaseTransactions;

    /**
     * @test
     */
    public function testHandle()
    {
        //Setup
        $user = factory(User::class)->create();

        //Mock and assert that functions are called
        $user_repo = Mockery::mock(UserRepository::class)
            ->shouldReceive(['delete' => null])
            ->mock();

        $service = new UserDeletionService($user_repo);
        $service->handle($user);
    }
}
