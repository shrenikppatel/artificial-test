<?php

namespace Tests\Unit;

use Artificial\Domain\Users\Contracts\UserRepository;
use Artificial\Domain\Users\Exceptions\InvalidPasswordException;
use Artificial\Domain\Users\Services\PasswordChangeService;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Mockery;
use Artificial\Domain\Users\Models\User;

/**
 * Class PasswordChangeServiceTest
 * @package Tests\Unit
 */
class PasswordChangeServiceTest extends TestCase
{
    use DatabaseMigrations, DatabaseTransactions;


    /**
     * @test
     */
    public function testHandle()
    {
        //Setup
        $user = factory(User::class)->create(['password' => passwordStub(123456)]);
        $new_password_string = 567890;

        //Mock and assert that functions are called
        $user_repo = Mockery::mock(UserRepository::class)
            ->shouldReceive(['updatePassword' => null])
            ->mock();

        $service = new PasswordChangeService($user_repo);
        $service->handle($user, "123456", $new_password_string);
    }

    /**
     * @test
     */
    public function testHandleWithWrongCurrentPassword()
    {
        //Setup
        $current_password = 123456;
        $user = factory(User::class)->create(['password' => passwordStub($current_password)]);
        $new_password_string = 567890;

        $service = app(PasswordChangeService::class);

        $this->expectException(InvalidPasswordException::class);
        $service->handle($user, 12345, $new_password_string);
    }
}
