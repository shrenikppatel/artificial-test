<?php

namespace Tests\Unit;

use Artificial\Domain\Users\Contracts\UserRepository;
use Artificial\Domain\Users\Exceptions\EmailTakenException;
use Artificial\Domain\Users\Exceptions\UsernameTakenException;
use Artificial\Domain\Users\Services\RegisterUserService;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Mockery;
use Faker\Factory;
use Artificial\Domain\Users\Models\User;

/**
 * Class RegisterUserServiceTest
 * @package Tests\Unit
 */
class RegisterUserServiceTest extends TestCase
{
    use DatabaseMigrations, DatabaseTransactions;


    /**
     * @test
     */
    public function testHandle()
    {
        //Setup
        $faker = Factory::create();

        $data = [
            'first_name' => $faker->firstName,
            'last_name' => $faker->lastName,
            'address' => $faker->address,
            'phone' => $faker->phoneNumber,
            'email' => $faker->email,
            'username' => "myusername",
            'password' => str_random(6)
        ];

        $user = factory(User::class)->create([
            'first_name' => firstNameStub($data['first_name']),
            'last_name' => lastNameStub($data['last_name']),
            'address' => addressStub($data['address']),
            'phone' => phoneStub($data['phone']),
            'email' => emailStub($data['email']),
            'username' => usernameStub($data['username']),
            'password' => passwordStub($data['password'])
        ]);

        //Mock and assert that functions are called
        $user_repo = Mockery::mock(UserRepository::class)
            ->shouldReceive(['doesEmailExist' => false, 'doesUsernameExist' => false, 'create' => $user])
            ->mock();

        $service = new RegisterUserService($user_repo);

        $service->handle($data);
    }

    /**
     * @test
     */
    public function testHandleWithEmailTaken()
    {
        //Setup
        $faker = Factory::create();

        $data = [
            'first_name' => $faker->firstName,
            'last_name' => $faker->lastName,
            'address' => $faker->address,
            'phone' => $faker->phoneNumber,
            'email' => $faker->email,
            'username' => $faker->userName,
            'password' => str_random(6)
        ];

        $user = factory(User::class)->create([
            'first_name' => firstNameStub($data['first_name']),
            'last_name' => lastNameStub($data['last_name']),
            'address' => addressStub($data['address']),
            'phone' => phoneStub($data['phone']),
            'email' => emailStub($data['email']),
            'username' => usernameStub($data['username']),
            'password' => passwordStub($data['password'])
        ]);

        //Mock and assert that functions are called
        $user_repo = Mockery::mock(UserRepository::class)
            ->shouldReceive(['doesEmailExist' => true, 'doesUsernameExist' => false, 'create' => $user])
            ->mock();

        $service = new RegisterUserService($user_repo);

        $this->expectException(EmailTakenException::class);
        $service->handle($data);
    }

    /**
     * @test
     */
    public function testHandleWithUsernameTaken()
    {
        //Setup
        $faker = Factory::create();

        $data = [
            'first_name' => $faker->firstName,
            'last_name' => $faker->lastName,
            'address' => $faker->address,
            'phone' => $faker->phoneNumber,
            'email' => $faker->email,
            'username' => $faker->userName,
            'password' => str_random(6)
        ];

        $user = factory(User::class)->create([
            'first_name' => firstNameStub($data['first_name']),
            'last_name' => lastNameStub($data['last_name']),
            'address' => addressStub($data['address']),
            'phone' => phoneStub($data['phone']),
            'email' => emailStub($data['email']),
            'username' => usernameStub($data['username']),
            'password' => passwordStub($data['password'])
        ]);

        //Mock and assert that functions are called
        $user_repo = Mockery::mock(UserRepository::class)
            ->shouldReceive(['doesEmailExist' => false, 'doesUsernameExist' => true, 'create' => $user])
            ->mock();

        $service = new RegisterUserService($user_repo);

        $this->expectException(UsernameTakenException::class);
        $service->handle($data);
    }
}
