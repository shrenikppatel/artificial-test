<?php

namespace Tests\Unit;

use Artificial\Domain\Users\Exceptions\UserNotFoundException;
use Artificial\Domain\Users\Models\User;
use Artificial\Infrastructure\DbUserRepository;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\DatabaseMigrations;

/**
 * Class DbUserRepositoryTest
 * @package Tests\Unit
 */
class DbUserRepositoryTest extends TestCase
{
    use DatabaseTransactions, DatabaseMigrations;

    /**
     * @test
     */
    public function testCreate()
    {
        $user_repo = app(DbUserRepository::class);

        $first_name = firstNameStub();
        $last_name = lastNameStub();
        $username = usernameStub();
        $phone = phoneStub();
        $email = emailStub();
        $password = passwordStub();
        $address = addressStub();

        $user = $user_repo->create(
            $first_name,
            $last_name,
            $username,
            $phone,
            $address,
            $email,
            $password
        );

        $this->assertInstanceOf(User::class, $user);
        $this->assertDatabaseHas('users', [
            'first_name' => (string) $first_name,
            'last_name' => (string) $last_name,
            'username' => (string) $username,
            'phone' => (string) $phone,
            'address' => (string) $address,
            'email' => (string) $email
            ]);
    }

    /**
     *
     */
    public function testUpdate()
    {
        $user_repo = app(DbUserRepository::class);

        $user = factory(User::class)->create();

        $first_name = firstNameStub();
        $last_name = lastNameStub();
        $username = usernameStub();
        $phone = phoneStub();
        $email = emailStub();
        $address = addressStub();

        $user_repo->update(
            $user,
            $first_name,
            $last_name,
            $username,
            $phone,
            $address,
            $email
        );

        $this->assertDatabaseHas('users', [
            'first_name' => (string) $first_name,
            'last_name' => (string) $last_name,
            'username' => (string) $username,
            'phone' => (string) $phone,
            'address' => (string) $address,
            'email' => (string) $email
        ]);
    }

    /**
     * @test
     */
    public function testFindById()
    {
        $user = factory(User::class)->create();
        $user_repo = app(DbUserRepository::class);

        $found_user = $user_repo->findById($user->id);

        $this->assertInstanceOf(User::class, $found_user);
        $this->assertEquals($found_user->id, $user->id);
    }

    /**
     * @test
     */
    public function testFindByIdWithInvalidId()
    {
        $user_repo = app(DbUserRepository::class);

        $this->expectException(UserNotFoundException::class);

        $new_value = $user_repo->findById(1);
    }

    /**
     * @test
     */
    public function testDoesEmailExistWithExistingEmail()
    {
        $user = factory(User::class)->create();

        $user_repo = app(DbUserRepository::class);

        $does_exist = $user_repo->doesEmailExist($user->email);

        $this->assertTrue($does_exist);
    }

    /**
     * @test
     */
    public function testDoesEmailExistWithExistingEmailExceptId()
    {
        $user = factory(User::class)->create();

        $user_repo = app(DbUserRepository::class);

        $does_exist = $user_repo->doesEmailExist($user->email, $user->id);

        $this->assertFalse($does_exist);
    }

    /**
     * @test
     */
    public function testDoesEmailExistWithNonExistingEmail()
    {
        $user_repo = app(DbUserRepository::class);

        $does_exist = $user_repo->doesEmailExist(emailStub("john@mail.com"));

        $this->assertFalse($does_exist);
    }

    /**
     * @test
     */
    public function testDoesEmailExistWithNonExistingEmailExceptId()
    {
        $user_repo = app(DbUserRepository::class);

        $does_exist = $user_repo->doesEmailExist(emailStub("john@mail.com"), 1);

        $this->assertFalse($does_exist);
    }

    /**
     * @test
     */
    public function testDoesUsernameExistWithExistingUsername()
    {
        $user = factory(User::class)->create();

        $user_repo = app(DbUserRepository::class);

        $does_exist = $user_repo->doesUsernameExist($user->username);

        $this->assertTrue($does_exist);
    }

    /**
     * @test
     */
    public function testDoesUsernameExistWithExistingUsernameExceptId()
    {
        $user = factory(User::class)->create();

        $user_repo = app(DbUserRepository::class);

        $does_exist = $user_repo->doesUsernameExist($user->username, $user->id);

        $this->assertFalse($does_exist);
    }

    /**
     * @test
     */
    public function testDoesUsernameExistWithNonExistingUsername()
    {
        $user_repo = app(DbUserRepository::class);

        $does_exist = $user_repo->doesUsernameExist(usernameStub());

        $this->assertFalse($does_exist);
    }

    /**
     * @test
     */
    public function testDoesUsernameExistWithNonExistingUsernameExceptId()
    {
        $user_repo = app(DbUserRepository::class);

        $does_exist = $user_repo->doesUsernameExist(usernameStub(), 1);

        $this->assertFalse($does_exist);
    }

    /**
     * @test
     */
    public function testUpdatePassword()
    {
        $user = factory(User::class)->create();
        $new_password = passwordStub(123456);
        $user_repo = app(DbUserRepository::class);

        $user_repo->updatePassword($user, $new_password);

        //Check Hash
        $success = password_verify(123456, $user->password);
    }

    /**
     * @test
     */
    public function testDelete()
    {
        $user = factory(User::class)->create();

        $this->assertDatabaseHas("users", ['id' => $user->id, "deleted_at" => null]);

        $user_repo = app(DbUserRepository::class);

        $user_repo->delete($user);

        $this->assertSoftDeleted("users", ['id' => $user->id]);
    }


}
