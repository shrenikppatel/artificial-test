<?php

namespace Tests\Unit;

use Artificial\Domain\Users\Contracts\UserRepository;
use Artificial\Infrastructure\DbUserRepository;
use Tests\TestCase;

/**
 * Class ContractsServiceProviderTest
 * @package Tests\Unit
 */
class ContractsServiceProviderTest extends TestCase
{
    /**
     * @test
     * @return void
     */
    public function itShouldResolveTheUserRepositoryContract()
    {
        $user_repo = resolve(UserRepository::class);
        $this->assertInstanceOf(DbUserRepository::class, $user_repo);
    }
}
