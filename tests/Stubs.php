<?php

function firstNameStub($first_name = null)
{
    $faker = \Faker\Factory::create();
    return new \Artificial\Domain\Users\ValueObjects\FirstName($first_name ? : $faker->firstName);
}

function lastNameStub($last_name = null)
{
    $faker = \Faker\Factory::create();
    return new \Artificial\Domain\Users\ValueObjects\LastName($last_name ? : $faker->lastName);
}

function phoneStub($phone = null)
{
    $faker = \Faker\Factory::create();
    return new \Artificial\Domain\Users\ValueObjects\Phone($phone ? : $faker->phoneNumber);
}

function addressStub($address = null)
{
    $faker = \Faker\Factory::create();
    return new \Artificial\Domain\Users\ValueObjects\Address($address ? : $faker->address);
}

function emailStub($email = null)
{
    $faker = \Faker\Factory::create();
    return new \Artificial\Domain\Users\ValueObjects\Email($email ? : $faker->email);
}

function usernameStub($username = null)
{
    $faker = \Faker\Factory::create();
    return new \Artificial\Domain\Users\ValueObjects\Username($username ? : str_random(6));
}

function passwordStub($password = null)
{
    $faker = \Faker\Factory::create();
    return new \Artificial\Domain\Users\ValueObjects\Password($password ? : $faker->password(6));
}