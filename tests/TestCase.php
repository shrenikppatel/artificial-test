<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Artificial\Domain\Users\Models\User;
use Tymon\JWTAuth\Facades\JWTAuth;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    public function makeToken(User $user)
    {
        $token = JWTAuth::fromUser($user);
        return $token;
    }
}
