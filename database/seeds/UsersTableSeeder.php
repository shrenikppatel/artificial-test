<?php

use Illuminate\Database\Seeder;
use Artificial\Domain\Users\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class)->create([
            'email' => emailStub("john.doe@gmail.com"),
            'username' => usernameStub("johndoe"),
            'password' => passwordStub("123456")
        ]);
    }
}
