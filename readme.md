## Artificial labs Test

This application is a backend for a user CRUD application. 

1. Registering a new user
2. Logging In in a user
3. Fetching a user
4. Updating user details
5. Updating user password
6. Deleting a user

## Requirements and Dependencies

The application is based on:   
1. PHP 7.1   
2. Sqlite   
3. Laravel 5.4    
4. PHPUnit 5.7     

## Installation

1. From zip file - Although this application uses Composer, there is no need for `Composer Install` as the zip file contains the vendor dir.

2. From bitbucket:

Run :   

`composer install`

Create .env file and set these values:   

APP_NAME=artificial    
APP_ENV=local    
APP_KEY=base64:KOOVXZwLGHHGWIQTk6nBi9qqzjnhUc/ursH4txvO/8Y=   
APP_DEBUG=true   
APP_LOG_LEVEL=debug   
APP_URL=http://localhost   
DB_CONNECTION=sqlite

## Running
To run the server, run the following command in the root dir:

`php artisan serve`

The database is seeded with a User.

The API uses JWT authentication and has the following endpoints:

1. POST "/api/auth/register" - To create a new user
2. POST "/api/auth/login" - To generate a JWT token and login the user
3. GET "/api/auth/refresh" - To refresh the token from a valid JWT user token
4. GET "/api/user" - To fetch user details
5. PUT "/api/user" - To update user details
6. PUT "/api/user/password" - To update user's password
7. DELETE "/api/user/" - To delete the user

Please check the - [Api Documentation](http://docs.artificialtest.apiary.io/)for the API documentation in detail.

To reset the application with the original data, run

`php artisan artificial:reset`

## Testing

Tests are included in /tests folder.

To test, run phpunit on the root folder if you have phpunit globally installed:

`phpunit`

To test with the phpunit local to the project, run the below cmd in the root folder:

`vendor/phpunit/phpunit/phpunit`

## Notes

This application is solely for the purpose of Artificial Labs Technical Test.

The code is structured following Domain Driven Design principles with keeping Application and Domain layer separate.    
The domain layer is in the /src dir whereas the application layer sits in the /app dir.
Few notes:

1. For a better adherence to Domain Driven Design, would switch use Doctrine instead of Eloquent.
2. For SPAs, would use Laravel's inbuilt Passport OAuth2 Password Grant instead of the current implementation of JWT
 so that the API can be secured by storing JWT tokens in cookies with the CSRF tokens.


Author : Shrenik Patel   
shrenikppatel@gmail.com


