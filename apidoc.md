FORMAT: 1A
HOST: Localhost

# Artificial Test

API for Artificial Techincal Test   
The database is seeded with a user, see below for login credentials   
@author shrenikppatel@gmail.com

# Group Auth

## Authorization
Most endpoints use JWT for authorization. In case the JWT is absent, expired or invalid, it shall throw a 400 error with the message. Check refresh token api for error responses.
Accepts the JWT in the authorization header in the form:  
Authorization : Bearer [jwt]   
The JWT token has an expiry of 1 hour and can be refreshed using the refresh service. It can only be refreshed for uptill 2 weeks after which a new token will need to be acquired


## Login [/api/auth/login]
Fetch jwt for Logged in API calls

### Login [POST]
The resource has the following attributes:
- email (string) [required] : Use 'john.doe@gmail.com'
- password (string) [required] : Use '123456'

+ Request (application/x-www-form-urlencoded)
    
        email=john.doe@gmail.com&password=123456
    
+ Response 200 (application/json)

        {
            "token": [JWT TOKEN]
        }

+ Response 403 (application/json)

        {
            "message":"Invalid credentials",
            "status_code":403
        }
        
+ Response 403 (application/json)

        {
            "message":"account_not_activated",
            "status_code":403,
        }

+ Response 422 (application/json)

    + Description
            
            Validation errors format
            
    + Body
        
            {
                "message":"422 Unprocessable Entity",
                "errors": {
                    "email":[
                    "The email field is required."
                    ]
                },
                "status_code":422
            }
        
## Refresh Token [/api/auth/refresh]
Refreshes the JWT token
### Refresh [GET]
The JWT token will be replaced in the header of the response along with the response body.

+ Request 

    + Headers

            Authorization: Bearer [JWT TOKEN]
            
+ Response 200

    + Headers
      
            Authorization: Bearer [refreshed JWT TOKEN]

    + Body
    
            {
                "token": [refreshed JWT TOKEN]
            }
            
+ Response 400

        {
            "message":"token_not_provided",
            "status_code":400
        }
        
+ Response 400

        {
            "message":"token_invalid",
            "status_code":400
        }
        
+ Response 400

        {
            "message":"token_expired",
            "status_code":400
        }

## Registration [/api/auth/register]
Register a new user
### Register [POST]
The resource has the following attributes:
- email (string) [required]
- password (string) [required|min:6|alpha_dash] - Minimum 6 characters, may have alpha-numeric characters, as well as dashes and underscores.
- username (string) [required|min:6|alpha_dash] - Minimum 6 characters, may have alpha-numeric characters, as well as dashes and underscores.
- phone (string) [required]
- first_name (string) [required|max:100] - 100 characters maximum
- last_name (string) [required|max:100] - 100 characters maximum
- address (string) [required] - 100 characters maximum

+ Request (application/x-www-form-urlencoded)
    
        email=john.doe@gmail.com&password=123456&username=john_doe&phone=07958972268&first_name=john&last_name=doe&address=london
    
+ Response 201 (application/json)
        
        + Description
        
                Empty Response. User is registered.
            
        + Body
        
                Empty Response
            

+ Response 422 (application/json)

        + Description
        
            Validation errors for each field
            
        + Body
        
            {
                "message":"Could not register a new user.",
                "errors":{
                    "first_name":[
                        "The first name field is required."
                    ],"last_name":[
                        "The last name field is required."
                    ],"username":[
                        "The username field is required."
                    ],"email":[
                        "The email field is required."
                    ],"password":[
                        "The password field is required."
                    ],"address":[
                        "The address field is required."
                    ],"phone":[
                        "The phone field is required."
                    ]
                },
                "status_code":422
            }
    
# Group User

## User [/api/user]

### Fetch User [GET]
Fetch the user details

+ Request 

    + Headers

            Authorization: Bearer [JWT TOKEN]
            
+ Response 200

        {
            "data":{
                "id":1,
                "first_name":"Roscoe",
                "last_name":"Gorczany",
                "email":"jalen47@lynch.com",
                "username":"fisyZC",
                "phone":"819-439-9228",
                "address":"895 Sylvester Motorway Apt. 781\nWest Friedrich, DE 57923",
                "created_at":"2017-05-14 20:18:44",
                "updated_at":"2017-05-14 20:18:44"
            }
        }
        
### Update User [PUT]

The resource has the following attributes:
- email (string) [required]
- username (string) [required|min:6|alpha_dash] - Minimum 6 characters, may have alpha-numeric characters, as well as dashes and underscores.
- phone (string) [required]
- first_name (string) [required|max:100] - 100 characters maximum
- last_name (string) [required|max:100] - 100 characters maximum
- address (string) [required] - 100 characters maximum

+ Request (application/x-www-form-urlencoded)

    + Headers
    
            Authorization: Bearer [JWT TOKEN]
            
    + Body
            
            email=john.doe@gmail.com&username=john_doe&phone=07958972268&first_name=john&last_name=doe&address=london
            
+ Response 204 (application/json)
        
+ Response 422 (application/json)

    + Description 
    
            Validation error
            
    + Body
    
            {
                "message":"Could not update the user.",
                "errors":{
                    "email":[
                        "Another user with the same email exists."
                    ]
                },
                "status_code":422
            }

### Delete User [DELETE]

+ Request (application/x-www-form-urlencoded)

    + Headers
    
            Authorization: Bearer [JWT TOKEN]
            
+ Response 204 (application/json)

# Group Password
## Password [api/user/password]

### Update Password [PUT]
The resource has the following attributes:
- current_password (string) [required]
- new_password (string) [required|min:6|alpha_dash] - Minimum 6 characters, may have alpha-numeric characters, as well as dashes and underscores.

+ Request (application/x-www-form-urlencoded)

    + Headers
    
            Authorization: Bearer [JWT TOKEN]
            
    + Body
    
            current_password=123456&new_password=567890
            
+ Response 204 (application/json)

+ Response 422 (application/json)

    + Description
    
            Validation error
            
    + Body
    
            {
                "message":"Could not update the user.",
                "errors":{
                    "email":[
                        "Another user with the same email exists."
                    ]
                },
                "status_code":422
            }
